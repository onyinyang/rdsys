package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"time"
)

type bridgedbMetrics struct {
	Metrics       metricsMap
	LastPublished time.Time
}

type metricsMap map[string]metricsData

type metricsData struct {
	PreviousValue      int
	ValueSincePublised int
}

func newBridgedbMetrics() *bridgedbMetrics {
	return &bridgedbMetrics{
		Metrics:       metricsMap{},
		LastPublished: time.Now(),
	}
}

func (bm *bridgedbMetrics) load(filePath string) error {
	f, err := os.Open(filePath)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return nil
		}
		return err
	}
	defer f.Close()

	decoder := json.NewDecoder(f)
	return decoder.Decode(bm)
}

func (bm *bridgedbMetrics) save(filePath string) error {
	f, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	decoder := json.NewEncoder(f)
	return decoder.Encode(bm)
}

func (bm *bridgedbMetrics) merge(newData map[string]int) {
	for key, value := range newData {
		data, ok := bm.Metrics[key]
		if !ok {
			bm.Metrics[key] = metricsData{value, 0}
			continue
		}

		increase := value - data.PreviousValue
		if increase < 0 {
			increase = value
		}
		bm.Metrics[key] = metricsData{value, data.ValueSincePublised + increase}
	}
}

func (bm *bridgedbMetrics) publish(filePath string, publishSeconds int) error {
	f, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	fmt.Fprintf(f, "bridgedb-metrics-end %s (%d s)\n", time.Now().Format("2006-01-02 15:04:05"), publishSeconds)
	fmt.Fprintln(f, "bridgedb-metrics-version 2")
	for key, data := range bm.Metrics {
		fmt.Fprintln(f, "bridgedb-metric-count", key, data.ValueSincePublised)
		data.ValueSincePublised = 0
		bm.Metrics[key] = data
	}

	bm.LastPublished = time.Now()
	return nil
}
