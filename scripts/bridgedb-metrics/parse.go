package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func parseMetrics(metrics io.Reader) (map[string]int, error) {
	res := map[string]int{}

	scanner := bufio.NewScanner(metrics)
	for scanner.Scan() {
		line := scanner.Text()
		if !strings.HasPrefix(line, metricName) {
			continue
		}

		labels, err := getMetricLabels(line)
		if err != nil {
			return nil, err
		}

		value, err := getMetricValue(line)
		if err != nil {
			return nil, err
		}

		res[metricKey(labels)] += value
	}

	return res, nil
}

func getMetricLabels(line string) (map[string]string, error) {
	labels := map[string]string{}

	parts := strings.Split(line, "{")
	if len(parts) == 1 {
		return labels, nil
	}
	if len(parts) != 2 {
		return nil, fmt.Errorf("Can't find the labels of a metric %s", line)
	}
	parts = strings.Split(parts[1], "} ")
	if len(parts) != 2 {
		return nil, fmt.Errorf("Can't find the labels of a metric %s", line)
	}

	for _, labelPair := range strings.Split(parts[0], ",") {
		parts = strings.Split(labelPair, "=")
		if len(parts) != 2 {
			return nil, fmt.Errorf("Malformed labels %s", line)
		}
		labels[parts[0]] = strings.Trim(parts[1], "\"")
	}

	return labels, nil
}

func getMetricValue(line string) (int, error) {
	parts := strings.Split(line, " ")
	return strconv.Atoi(parts[len(parts)-1])
}

func metricKey(labels map[string]string) string {
	distributor := labels["distributor"]
	transport := labels["transport"]

	var country string
	switch {
	case labels["country"] != "":
		country = labels["country"]
	case labels["lang"] != "":
		country = labels["lang"]
	case labels["provider"] != "":
		country = labels["provider"]
	default:
		country = "??"
	}

	return fmt.Sprintf("%s.%s.%s.success.none", distributor, transport, country)
}
