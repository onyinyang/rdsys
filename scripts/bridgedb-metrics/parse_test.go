package main

import (
	"fmt"
	"testing"
)

func TestMetricLabels(t *testing.T) {
	tests := map[string]map[string]string{
		"metric 10":                          {},
		"metric_foo{label=\"v\"} 12":         {"label": "v"},
		"my_metric{l1=\"v1\",l2=\"v2\"} 431": {"l1": "v1", "l2": "v2"},
	}

	for line, value := range tests {
		gotValue, err := getMetricLabels(line)
		if err != nil {
			t.Errorf("Got an error with line '%s': %v", line, err)
		}
		if fmt.Sprint(gotValue) != fmt.Sprint(value) {
			t.Errorf("Tested line '%s' and got %v instead of %v", line, gotValue, value)
		}
	}
}

func TestMetricValue(t *testing.T) {
	tests := map[string]int{
		"metric 10":                          10,
		"metric_foo{label=\"v\"} 12":         12,
		"metric_foo{label=\"v b\"} 18":       18,
		"my_metric{l1=\"v1\",l2=\"v2\"} 431": 431,
	}

	for line, value := range tests {
		gotValue, err := getMetricValue(line)
		if err != nil {
			t.Errorf("Got an error with line '%s': %v", line, err)
		}
		if gotValue != value {
			t.Errorf("Tested line '%s' and got %d instead of %d", line, gotValue, value)
		}
	}
}
