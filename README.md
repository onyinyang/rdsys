Rdsys
=====

**Disclaimer: This project is under construction.  Nothing is stable.**

Rdsys is short for *r*esource *d*istribution *sys*tem: Resources related to
censorship circumvention (e.g. proxies or download links) are handed out by a
variety of distribution methods to censored users.  The goal is to supply
censored users with circumvention proxies, allowing these users to join us on
the open Internet.

Rdsys supersedes
[BridgeDB](https://gitlab.torproject.org/tpo/anti-censorship/bridgedb).
Functionality-wise, rdsys and BridgeDB overlap but rdsys is neither a subset
nor a superset of BridgeDB's functionality.

Usage
=====

1. Compile everything:

        make build

2. Generate fake bridge descriptors:

        make descriptors

3. Start the backend by running:

        ./backend -config conf/config.json

4. Start a distributor, which distributes the backend's resources:

        ./distributors -name moat -config conf/config.json

5. Get some bridges:

        curl http://127.0.0.1:7500/moat/circumvention/defaults |jq

More documentation
==================

* [Design and architecture](doc/architecture.md)
* [Bridge distribution mechanisms](doc/distributors.md)
* [Resource testing](doc/resource-testing.md)
* [Implementing new distributors](doc/new-distributor.md)
* [i18n](doc/i18n.md)
